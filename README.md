<div align="center">

<h1> <a href="https://qiplex.com/software/speedy-duplicate-finder/">Speedy Duplicate Finder</a> </h1>
      
<h3> Extremely fast duplicate finder for Windows, Mac and Linux </h3>
    
You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/speedy-duplicate-finder/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/speedy-duplicate-finder/">my website</a>
<br>Code comes soon.
    
![Large Files Finder](https://qiplex.com/assets/img/app/main/speedy-duplicate-finder-app.png)
    
<h4>Check out the app features below: </h4>
  
![Speedy Duplicate Finder  - Features](https://user-images.githubusercontent.com/32670415/147223501-fd6da029-9b48-49f3-9414-160cc1bdb8b1.png)
      
 </div>
